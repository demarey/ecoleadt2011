// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include <line_info.h>

std::string line_info(std::string const & error, char const * file ,long line )
{
   std::stringstream s;
   s << "EXCEPTION: " << error << " in \"" << file << "\",line:" << line;
   return s.str();
}

