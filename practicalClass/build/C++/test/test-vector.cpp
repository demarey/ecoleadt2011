// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include <iostream>
#include <cpptest.h>
#include <stdlib.h>
#include <time.h>
#include "vector.h"

class TestAllocation : public Test::Suite
{
public:
	TestAllocation()
    {
		srand(time(NULL));
		TEST_ADD(TestAllocation::constructor)
		TEST_ADD(TestAllocation::resize)
    }

private:
	void constructor();
	void resize();
};

void TestAllocation::constructor()
{
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_NOTHING(Vector vector(rand()%999+1))
	TEST_THROWS_ANYTHING(Vector vector(0))
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_ANYTHING(Vector vector(-(rand()%999+1)))
}

void TestAllocation::resize()
{
	Vector vector(rand()%10+1);
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_NOTHING(vector.resize(rand()%999+1))
	TEST_THROWS_ANYTHING(vector.resize(0));
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_ANYTHING(vector.resize(-(rand()%999+1)))
}


int main()
{
  try
    {
      Test::Suite test;
      test.add(std::auto_ptr<Test::Suite>(new TestAllocation));
      Test::TextOutput output(Test::TextOutput::Verbose);
      if(!test.run(output))
	throw;
    }
  catch(...)
    {
      std::cout<<"\nThrow\n";
    }
}
