// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include <iostream>
#include <cpptest.h>
#include <stdlib.h>
#include <time.h>
#include "matrix.h"

class TestAllocation : public Test::Suite
{
public:
	TestAllocation()
    {
		srand(time(NULL));
		TEST_ADD(TestAllocation::constructor)
    }

private:
	void constructor();
	void resize();
};

void TestAllocation::constructor()
{
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_NOTHING(Matrix matrix(rand()%999+1,rand()%88+1))
	TEST_THROWS_ANYTHING(Matrix matrix(0,0))
	TEST_THROWS_ANYTHING(Matrix matrix(rand()%99+1,0))
	TEST_THROWS_ANYTHING(Matrix matrix(0,rand()%99+1))
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_ANYTHING(Matrix matrix(-(rand()%999+1),-(rand()%975+1)))
}

class TestOperation : public Test::Suite
{
public:
	TestOperation()
    {
		srand(time(NULL));
		TEST_ADD(TestOperation::setGet)
		TEST_ADD(TestOperation::egal)
		TEST_ADD(TestOperation::minus)
		TEST_ADD(TestOperation::vectorMultiplication)
    }

private:
	void setGet();
	void egal();
	void minus();
	void vectorMultiplication();
};

void TestOperation::setGet()
{
	Matrix matrix(rand()%10+1,rand()%15+1);
	TEST_THROWS_ANYTHING(matrix.set(matrix.getSizeRow(),matrix.getSizeCol()-1,rand()%9999))
	TEST_THROWS_ANYTHING(matrix.set(matrix.getSizeRow()-1,matrix.getSizeCol(),rand()%9999))
	TEST_THROWS_ANYTHING(matrix.set(matrix.getSizeRow(),matrix.getSizeCol(),rand()%9999))
	unsigned x=(matrix.getSizeRow()-1)%3,y=(matrix.getSizeCol()-1)%5;
	matrix.set(x,y,-145.678);
	if(matrix.get(x,y) != -145.678)
		TEST_FAIL("Error")
}

void TestOperation::egal()
{
	unsigned row=rand()%10+5,col=rand()%15+1;
	for(unsigned i=1;i<=row;i++)
		for(unsigned j=1;j<=col;j++)
		{
			Matrix matrix(row,col);
			Matrix copiedMatrix(i,j);
			if(i<matrix.getSizeRow() || j<matrix.getSizeCol())
				TEST_THROWS_ANYTHING(matrix=copiedMatrix)
			else
				TEST_THROWS_NOTHING(matrix=copiedMatrix)
		}
}

void TestOperation::minus()
{
	Matrix matrix(rand()%10+5,rand()%15+1);
	for(unsigned i=1;i<matrix.getSizeRow();i++)
		for(unsigned j=1;j<matrix.getSizeCol();j++)
		{
			Matrix tmpMatrix(i,j);
			TEST_THROWS_ANYTHING(matrix-tmpMatrix)
		}

	Matrix minusMatrix(matrix.getSizeRow(),matrix.getSizeCol());
	for(unsigned i=0;i<matrix.getSizeRow();i++)
		for(unsigned j=0;j<matrix.getSizeCol();j++)
		{
			matrix.set(i,j,15.54);
			minusMatrix.set(i,j,6.4);
		}
	TEST_THROWS_NOTHING(matrix-minusMatrix)
	for(unsigned i=0;i<matrix.getSizeRow();i++)
		for(unsigned j=0;j<matrix.getSizeCol();j++)
			TEST_ASSERT_DELTA(matrix.get(i,j), 9.14, 1e-6)
}

void TestOperation::vectorMultiplication()
{
	Matrix matrix(rand()%10+5,rand()%15+1);
	for(unsigned i=1;i<(matrix.getSizeCol()*2);i++)
	{
		Vector tmpVector(i);
		if(i != matrix.getSizeCol())
			TEST_THROWS_ANYTHING(matrix*tmpVector)
		else
			TEST_THROWS_NOTHING(matrix*tmpVector)
	}
	Matrix matrix2(3,3);
	matrix2.set(0,0,1);
	matrix2.set(0,1,5);
	matrix2.set(0,2,-3);
	matrix2.set(1,0,14);
	matrix2.set(1,1,56);
	matrix2.set(1,2,4);
	matrix2.set(2,0,0);
	matrix2.set(2,1,3);
	matrix2.set(2,2,-9);
	Vector vector(3);
	vector.set(0,2);
	vector.set(1,4);
	vector.set(2,8);
	Vector result(3);
	result= matrix2*vector;
	TEST_ASSERT_DELTA(result.get(0),-2,1e-6)
	TEST_ASSERT_DELTA(result.get(1),284,1e-6)
	TEST_ASSERT_DELTA(result.get(2),-60,1e-6)
}


int main()
{
  try
    {
      Test::Suite test;
      test.add(std::auto_ptr<Test::Suite>(new TestAllocation));
      test.add(std::auto_ptr<Test::Suite>(new TestOperation));
      Test::TextOutput output(Test::TextOutput::Verbose);
      if(!test.run(output))
	throw;
    }
  catch(...)
    {
      std::cout<<"\nThrow\n";
    }
}
