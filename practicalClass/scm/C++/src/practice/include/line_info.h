// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#ifndef LINE_INFO_H
#define LINE_INFO_H

#include <stdexcept>
#include <iostream>
#include <sstream>

//! @fn line_info(std::string const & error, char const * file ,long line )
//! @brief to give the line of errors
std::string line_info(std::string const & error, char const * file ,long line );

#endif
