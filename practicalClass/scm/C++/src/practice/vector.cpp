// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include "vector.h"
#include "line_info.h"

Vector::Vector(unsigned _size)
{
	try
	{
		if(_size == 0)
			throw 0;
		//! @bug cast
		// unsigned var=-1; => var = 4 294 967 295
		// so, Vector v(-1) <=> Vector v(4 294 967 295)
		if(_size > 1e6)
		// 1e6, for example...
			throw 1;
		for(unsigned i=0;i<_size;i++)
			vector.push_back(0.);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::Vector(unsigned _size)",__FILE__,__LINE__));
	}
}

Vector::Vector(const Vector & _vector)
{
	vector.clear();
	vector=_vector.getRef();
}

Vector::~Vector()
{

}

void Vector::set(unsigned _i , double _value)
{
	try
	{
		if (_i >= vector.size())
			throw 0;
		vector.at(_i)=_value;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::set(unsigned _i , double _value)",__FILE__,__LINE__));
	}
}

double Vector::get(unsigned _i)
{
	try
	{
		if (_i >= vector.size())
			throw 0;
		return vector.at(_i);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::get(unsigned _i)",__FILE__,__LINE__));
	}
}

unsigned Vector::getSize()
{
	return vector.size();
}

unsigned Vector::getSize() const
{
	return vector.size();
}

void Vector::resize(unsigned _size)
{
	try
	{
		if(_size == 0)
			throw 0;
		if(_size > 1e6)
			throw 1;
		vector.clear();
		for(unsigned i=0;i<_size;i++)
			vector.push_back(0.);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::resize(unsigned _size)",__FILE__,__LINE__));
	}
}

void Vector::print()
{
	std::cout<<"\n\n";
	for(std::vector<double>::iterator it = vector.begin(); it!=vector.end(); ++it)
		std::cout << *it << std::endl;
	std::cout<<"\n\n";
}

std::vector<double> const & Vector::getRef() const
{
	return vector;
}

Vector & Vector::operator=(Vector const & _vector)
{
	try
	{
		if(getSize() != _vector.getSize())
			throw 0;
		vector.clear();
		vector=_vector.getRef();
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::operator=(Vector & _vector)",__FILE__,__LINE__));
	}
}

Vector & Vector::operator+(Vector & _vector)
{
	try
	{
		if(getSize() != _vector.getSize())
			throw 0;
		for(unsigned i=0;i<getSize();i++)
			set(i,get(i)+_vector.get(i));
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::operator+(Vector & _vector)",__FILE__,__LINE__));
	}
}

Vector & Vector::operator-(Vector & _vector)
{
	try
	{
		if(vector.size() != _vector.getSize())
			throw 0;
		for(unsigned i=0;i<getSize();i++)
			set(i,get(i)-_vector.get(i));
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Vector::operator-(Vector & _vector)",__FILE__,__LINE__));
	}
}
