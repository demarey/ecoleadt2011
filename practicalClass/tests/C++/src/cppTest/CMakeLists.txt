INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src/cppTest)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib)
SET(LIST
          collectoroutput.cpp
          compileroutput.cpp
          htmloutput.cpp
          missing.cpp
          source.cpp
          suite.cpp
          textoutput.cpp
          time.cpp
          utils.cpp
)
ADD_LIBRARY(cpptest SHARED ${LIST})
