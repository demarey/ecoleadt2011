// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#ifndef VECTOR_H
#define VECTOR_H

#include <vector>

//! @class Vector
//! @brief Declaration of the class representing a Vector
//! @version 1.0
//! @date 2011
//! @author SED Lille
class Vector
{
public:

	//! @brief Constructor
	//! @param _size size of vector
	Vector(unsigned _size);

	//! @brief Constructor
	//! @param _vector copied vector
	Vector(const Vector & _vector);

	//! @brief Destructor
	~Vector();

	//! @brief To set the value of the i-th element to _value
	//! @param _i i-th element
	//! @param _value new value
	void set(unsigned _i , double _value);

	//! @brief To return the i-th element
	//! @param _i i-th element
	//! @return value of i-th element
	double get(unsigned _i);

	//! @brief To return the size
	//! @return size
	unsigned getSize();

	//! @brief To return the size
	//! @return size
	unsigned getSize() const;

	//! @brief To resize the vector
	//! @param _size number of elements
	void resize(unsigned _size);

	//! @brief To print the values
	void print();

	//! @brief To get the vector's reference
	std::vector<double> const & getRef() const;

	//! @brief myVector = _vector
	//! @param _vector
	Vector & operator=(Vector const & _vector);

	//! @brief myVector + _vector: myVector = myVector + _vector
	//! @param _vector
	Vector & operator+(Vector & _vector);

	//! @brief myVector - _vector: myVector = myVector - _vector
	//! @param _vector
	Vector & operator-(Vector & _vector);



private:

	//! @param vector
	std::vector <double> vector;

};

#endif
