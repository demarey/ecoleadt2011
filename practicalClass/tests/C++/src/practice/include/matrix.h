// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#ifndef MATRIX_H
#define MATRIX_H

#include "vector.h"

//! @class Matrix
//! @brief Declaration of the class representing a Matrix
//! @version 1.0
//! @date 2011
//! @author SED Lille
class Matrix
{
public:

	//! @brief Constructor
	//! @param _sizeRow size of rows
	//! @param _sizeCol size of columns
	Matrix(unsigned _sizeRow , unsigned _sizeCol);

	//! @brief Constructor
	//! @param _matrix copied matrix
	Matrix(const Matrix & _matrix);

	//! @brief Destructor
	~Matrix();

	//! @brief To return the number of the rows
	//! @return size of rows
	unsigned getSizeRow() const;

	//! @brief To return the number of the columns
	//! @return size of columns
	unsigned getSizeCol() const;

	//! @brief To return the element (i,j)
	//! @param _i i-th element (row)
	//! @param _j j-th element (column)
	//! @return value of element (i,j)
	double get(unsigned _i, unsigned _j) const;

	//! @brief To set the value of the element (i,j) to _value
	//! @param _i i-th element (row)
	//! @param _j j-th element (column)
	//! @param _value new value
	void set(unsigned _i , unsigned _j , double _value);

	//! @brief To print the values
	void print();

	//! @brief To get the matrix's reference
	std::vector<double> const & getRef() const;

	//! @brief myMatrix = _matrix
	//! @param _matrix
	Matrix & operator=(Matrix & _matrix);

	//! @brief myMatrix + _matrix: myMatrix = myMatrix + _matrix
	//! @param _matrix
	Matrix & operator+(Matrix & _matrix);

	//! @brief myMatrix - _matrix: myMatrix = myMatrix - _matrix
	//! @param _matrix
	Matrix & operator-(Matrix & _matrix);

	//! @brief newVector = myMatrix * _vector
	//! @param _vector
	//! @return myMatrix * _vector
	Vector operator*(Vector & _vector);

private:

	//! @param matrix
	std::vector <double> matrix;

	//! @param sizeRow
	unsigned sizeRow;

	//! @param sizeCol
	unsigned sizeCol;
};

#endif
