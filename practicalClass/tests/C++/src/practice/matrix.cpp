// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include "matrix.h"
#include "line_info.h"

Matrix::Matrix(unsigned _sizeRow , unsigned _sizeCol):sizeRow(_sizeRow),sizeCol(_sizeCol)
{
	try
	{
		if(sizeRow == 0 || sizeCol == 0)
			throw 0;
		if(sizeRow > 1e6 || sizeCol > 1e6)
			throw 1;
		for(unsigned i=0;i<sizeRow*sizeCol;i++)
			matrix.push_back(0.);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::Matrix(unsigned _sizeRow , unsigned _sizeCol)",__FILE__,__LINE__));
	}
}

Matrix::Matrix(const Matrix & _matrix)
{
	try
	{
		sizeRow=_matrix.getSizeRow();
		sizeCol=_matrix.getSizeCol();
		for(unsigned i=0;i<sizeRow;i++)
			for(unsigned j=0;j<sizeRow;j++)
				matrix.at(i*sizeCol+j)=_matrix.get(i,j);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::Matrix(Matrix & _matrix)",__FILE__,__LINE__));
	}
}

Matrix::~Matrix()
{

}

unsigned Matrix::getSizeRow() const
{
	return sizeRow;
}

unsigned Matrix::getSizeCol() const
{
	return sizeCol;
}

double Matrix::get(unsigned _i, unsigned _j) const
{
	try
	{
		if(_i >= sizeRow || _j >= sizeCol)
			throw 0;
		return matrix.at(_i*sizeCol+_j);
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::get(unsigned _i, unsigned _j)",__FILE__,__LINE__));
	}
}

void Matrix::set(unsigned _i , unsigned _j , double _value)
{
	try
	{
		if(_i >= sizeRow || _j >= sizeCol)
			throw 0;
		matrix.at(_i*sizeCol+_j)=_value;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::set(unsigned _i , unsigned _j , double _value)",__FILE__,__LINE__));
	}
}

void Matrix::print()
{
	std::cout<<"\n";
	for(unsigned i=0;i<sizeRow;i++)
	{
		std::cout<<"\n";
		for(unsigned j=0;j<sizeCol;j++)
			std::cout<<"\t"<<get(i,j);
	}
	std::cout<<"\n\n";
}

std::vector <double> const & Matrix::getRef() const
{
	return matrix;
}

Matrix & Matrix::operator=(Matrix & _matrix)
{
	try
	{
		if(sizeRow != _matrix.getSizeRow() || sizeCol != _matrix.getSizeCol())
			throw 0;
		matrix=_matrix.getRef();
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::operator=(Matrix & _matrix)",__FILE__,__LINE__));
	}
}

Matrix & Matrix::operator+(Matrix & _matrix)
{
	try
	{
		if(sizeRow != _matrix.getSizeRow() || sizeCol != _matrix.getSizeCol())
			throw 0;
		for(unsigned i=0;i<sizeRow;i++)
			for(unsigned j=0;j<sizeCol;j++)
				set(i,j,get(i,j)+_matrix.get(i,j));
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::operator+(Matrix & _matrix)",__FILE__,__LINE__));
	}
}

Matrix & Matrix::operator-(Matrix & _matrix)
{
	try
	{
		if(sizeRow != _matrix.getSizeRow() || sizeCol != _matrix.getSizeCol())
			throw 0;
		for(unsigned i=0;i<sizeRow;i++)
			for(unsigned j=0;j<sizeCol;j++)
				set(i,j,get(i,j)-_matrix.get(i,j));
		return *this;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::operator-(Matrix & _matrix)",__FILE__,__LINE__));
	}
}

Vector Matrix::operator*(Vector & _vector)
{
	try
	{
		if(sizeCol != _vector.getSize())
			throw 0;
		Vector tmp(sizeRow);
		for(unsigned i=0;i<sizeRow;i++)
			for(unsigned k=0;k<sizeCol;k++)
				tmp.set(i,tmp.get(i)+get(i,k)*_vector.get(k));
		return tmp;
	}
	catch(...)
	{
		throw std::domain_error( line_info( "Matrix::operator*(Vector & _vector)",__FILE__,__LINE__));
	}
}

