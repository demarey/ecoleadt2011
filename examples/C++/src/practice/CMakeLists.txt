# Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info

include_directories(${SRC_C++}/practice/include)

set(SOURCES
                line_info.cpp
                vector.cpp
                matrix.cpp
	            )
	                   	
add_library(practice SHARED ${SOURCES})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib)
