# Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info

include_directories(${CMAKE_SOURCE_DIR}/src/practice/include)
include_directories(${CMAKE_SOURCE_DIR}/src/cppTest)

set(TEST_LIST    
                test-vector
                test-matrix
                )

foreach(i ${TEST_LIST})
    add_executable("t-${i}" "${i}.cpp")
    target_link_libraries("t-${i}" cpptest practice)
    add_test("UnitTesting-${i}" "t-${i}" )
endforeach(i)
