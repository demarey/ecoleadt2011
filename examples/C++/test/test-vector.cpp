// Practice class: Don't make software like that!

// Copyright (C) by INRIA, SED Lille - 2011 - CECILL License V2 - http://www.cecill.info/

#include <iostream>
#include <cpptest.h>
#include <stdlib.h>
#include <time.h>
#include "vector.h"

class TestAllocation : public Test::Suite
{
public:
	TestAllocation()
    {
		srand(time(NULL));
		TEST_ADD(TestAllocation::constructor)
		TEST_ADD(TestAllocation::resize)
    }

private:
	void constructor();
	void resize();
};

void TestAllocation::constructor()
{
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_NOTHING(Vector vector(rand()%999+1))
	TEST_THROWS_ANYTHING(Vector vector(0))
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_ANYTHING(Vector vector(-(rand()%999+1)))
}

void TestAllocation::resize()
{
	Vector vector(rand()%10+1);
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_NOTHING(vector.resize(rand()%999+1))
	TEST_THROWS_ANYTHING(vector.resize(0));
	for(int i=0;i<rand()%50+1;i++)
		TEST_THROWS_ANYTHING(vector.resize(-(rand()%999+1)))
}


class TestOperation : public Test::Suite
{
public:
	TestOperation()
    {
		srand(time(NULL));
		TEST_ADD(TestOperation::setGet)
		TEST_ADD(TestOperation::egal)
		TEST_ADD(TestOperation::minus)
    }

private:
	void setGet();
	void egal();
	void minus();
};

void TestOperation::setGet()
{
	Vector vector(rand()%10+1);
	TEST_THROWS_ANYTHING(vector.set(vector.getSize(),rand()%9999))
	TEST_THROWS_ANYTHING(vector.get(vector.getSize()+rand()%99))
	unsigned position=(vector.getSize()-1)%3;
	vector.set(position,456.12);
	if(vector.get(position) != 456.12)
		TEST_FAIL("Error")
}

void TestOperation::egal()
{
	Vector vector(rand()%10+5);
	for(unsigned i=1;i<=vector.getSize();i++)
	{
		Vector copiedVector(i);
		if(i<vector.getSize())
			TEST_THROWS_ANYTHING(vector=copiedVector)
		else
			TEST_THROWS_NOTHING(vector=copiedVector)
	}
}

void TestOperation::minus()
{
	Vector vector(rand()%10+5);
	for(unsigned i=1;i<vector.getSize();i++)
	{
		Vector tmpVector(i);
		TEST_THROWS_ANYTHING(vector-tmpVector)
	}
	Vector minusVector(vector.getSize());
	for(unsigned i=0;i<vector.getSize();i++)
	{
		vector.set(i,i+1.55);
		minusVector.set(i,9.665);
	}
	TEST_THROWS_NOTHING(vector-minusVector)
	for(unsigned i=0;i<vector.getSize();i++)
		TEST_ASSERT(vector.get(i)  - i - 1.55 == -9.665)
}

int main()
{
  try
    {
      Test::Suite test;
      test.add(std::auto_ptr<Test::Suite>(new TestAllocation));
      test.add(std::auto_ptr<Test::Suite>(new TestOperation));
      Test::TextOutput output(Test::TextOutput::Verbose);
      if(!test.run(output))
	throw;
    }
  catch(...)
    {
      std::cout<<"\nThrow\n";
    }
}
